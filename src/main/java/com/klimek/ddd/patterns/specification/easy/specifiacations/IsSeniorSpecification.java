package com.klimek.ddd.patterns.specification.easy.specifiacations;

import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.easy.traditional.Person;

public class IsSeniorSpecification extends CompositeSpecification<Person> {
    private static final Integer MINIMAL_SENIOR_AGE = 65;

    @Override
    public boolean isSatisfiedBy(Person person) {
        return person.getAge() > MINIMAL_SENIOR_AGE;
    }
}
