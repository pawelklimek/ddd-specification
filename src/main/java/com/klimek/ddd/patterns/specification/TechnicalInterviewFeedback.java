package com.klimek.ddd.patterns.specification;

public class TechnicalInterviewFeedback {

    private final boolean isPositive;

    public TechnicalInterviewFeedback(boolean pIsPositive) {
        isPositive = pIsPositive;
    }

    public boolean isPositive() {
        return isPositive;
    }
}
