package com.klimek.ddd.patterns.specification;

public class Candidate {
    private final int minimalSalary;

    public Candidate(int pMinimalHourlyWage) {
        this.minimalSalary = pMinimalHourlyWage;
    }

  public int getProposeSalary() {
        return minimalSalary;
    }
}
