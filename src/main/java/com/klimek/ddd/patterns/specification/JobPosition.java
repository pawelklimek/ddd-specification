package com.klimek.ddd.patterns.specification;

public class JobPosition {
    private final int maxSalary;
    private final String name;

   public JobPosition(final int pMaxSalary, final String pName) {
        this.maxSalary = pMaxSalary;
       this.name = pName;
   }

    public int getMaxSalary() {
        return maxSalary;
    }

    public String getName() {
        return name;
    }
}
