package com.klimek.ddd.patterns.specification;

public class RecruitmentProcess {
    private final Candidate candidate;
    private final JobPosition jobPosition;
    private final SoftSkillsInterviewFeedback softSkillsInterviewFeedback;
    private final TechnicalInterviewFeedback technicalInterviewFeedback;
    private final TeamLeaderFeedback teamLeaderFeedback;

    public RecruitmentProcess(Candidate pCandidate,
                              JobPosition pJobPosition,
                              SoftSkillsInterviewFeedback pSoftSkillsInterviewFeedback,
                              TechnicalInterviewFeedback pTechnicalInterviewFeedback,
                              TeamLeaderFeedback pTeamLeaderFeedback) {
        candidate = pCandidate;
        jobPosition = pJobPosition;
        softSkillsInterviewFeedback = pSoftSkillsInterviewFeedback;
        technicalInterviewFeedback = pTechnicalInterviewFeedback;
        teamLeaderFeedback = pTeamLeaderFeedback;
    }

    public boolean isSuccess() {
        if (jobPosition.getName().equals("DEVELOPER"))
        {
            return developerRecruitmentProcess();
        }
        if (jobPosition.getName().equals("CLEANER"))
        {
            return cleanerRecruitmentProcess();
        }
        if (jobPosition.getName().equals("TEAM LEADER"))
        {
            return teamLeaderRecruitmentProcess();
        }
        return false;
    }

    private boolean cleanerRecruitmentProcess() {
        return softSkillsInterviewFeedback.isPositive()
                && candidate.getProposeSalary() < jobPosition.getMaxSalary();
    }

    private boolean developerRecruitmentProcess() {
        return softSkillsInterviewFeedback.isPositive()
                && candidate.getProposeSalary() < jobPosition.getMaxSalary()
                && technicalInterviewFeedback.isPositive()
                && teamLeaderFeedback.isPositive();
    }

    private boolean teamLeaderRecruitmentProcess() {
        return softSkillsInterviewFeedback.isPositive()
                && candidate.getProposeSalary() < jobPosition.getMaxSalary()
                && technicalInterviewFeedback.isPositive();
    }
}
