package com.klimek.ddd.patterns.specification.easy.specifiacations;

import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.easy.traditional.Person;

public class IsUnderageSpecification extends CompositeSpecification<Person> {
    private static final Integer MAJORITY_AGE = 18;

    @Override
    public boolean isSatisfiedBy(Person person) {
        return person.getAge() < MAJORITY_AGE;
    }
}
