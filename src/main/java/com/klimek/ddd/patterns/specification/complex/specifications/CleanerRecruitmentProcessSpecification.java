package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.Specification;

public class CleanerRecruitmentProcessSpecification implements Specification<RecruitmentProcess> {

    Specification<RecruitmentProcess> softSkillsFeedbackSuccessful = new SoftSkillsFeedbackSpecification();
    Specification<RecruitmentProcess> salaryInRange = new SalarySpecification();

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        JobNameSpecification cleanerJobName = new JobNameSpecification("CLEANER");
        return cleanerJobName
                .and(softSkillsFeedbackSuccessful)
                .and(salaryInRange)
                .isSatisfiedBy(recruitmentProcess);
    }
}
