package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.shared.Specification;

public class RecruitmentProcessSpecification extends CompositeSpecification<RecruitmentProcess> {

    CompositeSpecification<RecruitmentProcess> developerRecruitmentProcess = new DeveloperRecruitmentProcessSpecification();
    Specification<RecruitmentProcess> teamLeaderRecruitmentProcess = new TeamLeaderRecruitmentProcessSpecification();
    Specification<RecruitmentProcess> cleanerRecruitmentProcess = new CleanerRecruitmentProcessSpecification();

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        return developerRecruitmentProcess
                .or(teamLeaderRecruitmentProcess)
                .or(cleanerRecruitmentProcess)
                .isSatisfiedBy(recruitmentProcess);
    }
}
