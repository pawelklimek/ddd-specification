package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;

public class TeamLeaderFeedbackSpecification extends CompositeSpecification<RecruitmentProcess> {

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        return recruitmentProcess.getTeamLeaderFeedback().isPositive();
    }
}
