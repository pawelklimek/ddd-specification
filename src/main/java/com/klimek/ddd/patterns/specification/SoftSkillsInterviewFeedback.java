package com.klimek.ddd.patterns.specification;

public class SoftSkillsInterviewFeedback {

    private final boolean isPositive;

   public SoftSkillsInterviewFeedback(boolean pIsPositive) {
        isPositive = pIsPositive;
    }

    public boolean isPositive() {
        return isPositive;
    }
}
