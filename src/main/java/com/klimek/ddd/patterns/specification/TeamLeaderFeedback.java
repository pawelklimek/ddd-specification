package com.klimek.ddd.patterns.specification;

public class TeamLeaderFeedback {

    private final boolean isPositive;

    public TeamLeaderFeedback(boolean pPIsPositive) {
        isPositive = pPIsPositive;
    }

    public boolean isPositive() {
        return isPositive;
    }
}
