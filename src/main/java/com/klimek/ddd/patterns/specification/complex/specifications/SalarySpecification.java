package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.Specification;

public class SalarySpecification implements Specification<RecruitmentProcess> {

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        return recruitmentProcess.getCandidate().getProposeSalary() < recruitmentProcess.getJobPosition().getMaxSalary();
    }
}

