package com.klimek.ddd.patterns.specification.easy.traditional;

public class Person {
    Integer age;

    public Person(Integer pAge) {
        this.age = pAge;
    }

    public Integer getAge() {
        return age;
    }
}
