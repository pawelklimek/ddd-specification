package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;

public class JobNameSpecification extends CompositeSpecification<RecruitmentProcess> {

    private final String jobName;

    public JobNameSpecification(String pJobName) {
        jobName = pJobName;
    }

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        return recruitmentProcess
                .getJobPosition()
                .getName()
                .equals(jobName);
    }
}

