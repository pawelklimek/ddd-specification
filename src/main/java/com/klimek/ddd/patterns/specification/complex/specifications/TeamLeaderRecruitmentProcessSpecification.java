package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.Specification;

public class TeamLeaderRecruitmentProcessSpecification implements Specification<RecruitmentProcess> {

    Specification<RecruitmentProcess> softSkillsFeedbackSuccessful = new SoftSkillsFeedbackSpecification();
    Specification<RecruitmentProcess> salaryInRange = new SalarySpecification();
    Specification<RecruitmentProcess> technicalInterviewFeedbackSuccessful = new TechnicalInterviewFeedbackSpecification();

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        JobNameSpecification teamLeaderJobName = new JobNameSpecification("TEAM LEADER");
        return teamLeaderJobName
                .and(softSkillsFeedbackSuccessful)
                .and(salaryInRange)
                .and(technicalInterviewFeedbackSuccessful)
                .isSatisfiedBy(recruitmentProcess);
    }
}
