package com.klimek.ddd.patterns.specification.complex.traditional;

import com.klimek.ddd.patterns.specification.Candidate;
import com.klimek.ddd.patterns.specification.JobPosition;
import com.klimek.ddd.patterns.specification.SoftSkillsInterviewFeedback;
import com.klimek.ddd.patterns.specification.TeamLeaderFeedback;
import com.klimek.ddd.patterns.specification.TechnicalInterviewFeedback;

public class RecruitmentProcess {

    private final Candidate candidate;
    private final JobPosition jobPosition;
    private final SoftSkillsInterviewFeedback softSkillsInterviewFeedback;
    private final TechnicalInterviewFeedback technicalInterviewFeedback;
    private final TeamLeaderFeedback teamLeaderFeedback;

    RecruitmentProcess(Candidate pCandidate,
                       JobPosition pJobPosition,
                       SoftSkillsInterviewFeedback pSoftSkillsInterviewFeedback,
                       TechnicalInterviewFeedback pTechnicalInterviewFeedback,
                       TeamLeaderFeedback pTeamLeaderFeedback) {
        candidate = pCandidate;
        jobPosition = pJobPosition;
        softSkillsInterviewFeedback = pSoftSkillsInterviewFeedback;
        technicalInterviewFeedback = pTechnicalInterviewFeedback;
        teamLeaderFeedback = pTeamLeaderFeedback;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public JobPosition getJobPosition() {
        return jobPosition;
    }

    public SoftSkillsInterviewFeedback getSoftSkillsInterviewFeedback() {
        return softSkillsInterviewFeedback;
    }

    public TechnicalInterviewFeedback getTechnicalInterviewFeedback() {
        return technicalInterviewFeedback;
    }

    public TeamLeaderFeedback getTeamLeaderFeedback() {
        return teamLeaderFeedback;
    }
}
