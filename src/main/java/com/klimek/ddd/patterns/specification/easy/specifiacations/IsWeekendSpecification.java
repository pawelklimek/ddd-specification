package com.klimek.ddd.patterns.specification.easy.specifiacations;

import java.time.DayOfWeek;
import java.time.LocalDate;

import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.easy.traditional.Person;

public class IsWeekendSpecification extends CompositeSpecification<Person> {

    @Override
    public boolean isSatisfiedBy(Person person) {
        DayOfWeek today = LocalDate.now().getDayOfWeek();
        return today == DayOfWeek.SATURDAY
                || today == DayOfWeek.SUNDAY;
    }
}
