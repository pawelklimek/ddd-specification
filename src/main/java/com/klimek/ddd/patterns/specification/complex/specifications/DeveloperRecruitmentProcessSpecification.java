package com.klimek.ddd.patterns.specification.complex.specifications;

import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.shared.Specification;

public class DeveloperRecruitmentProcessSpecification extends CompositeSpecification<RecruitmentProcess> {

    Specification<RecruitmentProcess> softSkillsFeedbackSuccessful = new SoftSkillsFeedbackSpecification();
    Specification<RecruitmentProcess> salaryInRange = new SalarySpecification();
    Specification<RecruitmentProcess> technicalInterviewFeedbackSuccessful = new TechnicalInterviewFeedbackSpecification();
    Specification<RecruitmentProcess> teamLeaderFeedbackSuccessful = new TeamLeaderFeedbackSpecification();

    @Override
    public boolean isSatisfiedBy(RecruitmentProcess recruitmentProcess) {
        JobNameSpecification jobNameDeveloper = new JobNameSpecification("DEVELOPER");
        return jobNameDeveloper
                .and(softSkillsFeedbackSuccessful)
                .and(salaryInRange)
                .and(teamLeaderFeedbackSuccessful)
                .and(technicalInterviewFeedbackSuccessful)
                .isSatisfiedBy(recruitmentProcess);
    }
}
