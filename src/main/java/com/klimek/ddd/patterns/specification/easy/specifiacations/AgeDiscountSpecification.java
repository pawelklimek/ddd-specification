package com.klimek.ddd.patterns.specification.easy.specifiacations;


import com.klimek.ddd.patterns.specification.shared.CompositeSpecification;
import com.klimek.ddd.patterns.specification.easy.traditional.Person;

public class AgeDiscountSpecification extends CompositeSpecification<Person> {

    CompositeSpecification<Person> isUnderage = new IsUnderageSpecification();
    CompositeSpecification<Person> isSenior = new IsSeniorSpecification();
    CompositeSpecification<Person> isWeekend = new IsWeekendSpecification();

    @Override
    public boolean isSatisfiedBy(Person person) {
        return isUnderage
                .and(isWeekend)
                .or(isSenior)
                .isSatisfiedBy(person);
    }
}
