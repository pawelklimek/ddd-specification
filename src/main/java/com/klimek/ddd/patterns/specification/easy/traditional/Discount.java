package com.klimek.ddd.patterns.specification.easy.traditional;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Discount {
    private static final Integer MAJORITY_AGE = 18;
    private static final Integer MINIMAL_SENIOR_AGE = 65;

    public static boolean isDiscount(Person person) {
        return person.getAge() < MAJORITY_AGE && isWeekend()
                || person.getAge() > MINIMAL_SENIOR_AGE;
    }

    private static boolean isWeekend() {
        DayOfWeek today = LocalDate.now().getDayOfWeek();
        return today == DayOfWeek.SATURDAY
                || today == DayOfWeek.SUNDAY;
    }
}
