package com.klimek.ddd.patterns.specification.shared;

public interface Specification<T> {
    boolean isSatisfiedBy(T t);
}
