package com.klimek.ddd.patterns.specification.ddd;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.klimek.ddd.patterns.specification.Candidate;
import com.klimek.ddd.patterns.specification.JobPosition;
import com.klimek.ddd.patterns.specification.SoftSkillsInterviewFeedback;
import com.klimek.ddd.patterns.specification.TeamLeaderFeedback;
import com.klimek.ddd.patterns.specification.TechnicalInterviewFeedback;
import com.klimek.ddd.patterns.specification.complex.traditional.RecruitmentProcess;
import com.klimek.ddd.patterns.specification.complex.specifications.RecruitmentProcessSpecification;

@SpringBootTest
class RecruitmentProcessTest {

    Candidate candidate = new Candidate(10000);
    SoftSkillsInterviewFeedback soft = new SoftSkillsInterviewFeedback(true);
    TechnicalInterviewFeedback technical = new TechnicalInterviewFeedback(true);
    TeamLeaderFeedback teamLeader = new TeamLeaderFeedback(true);

    @Test
    void developer() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "DEVELOPER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, teamLeader);

        //WHEN
        boolean result = new RecruitmentProcessSpecification().isSatisfiedBy(recruitmentProcess);

        //THEN
        Assertions.assertTrue(result);
    }

    @Test
    void team_lead() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "TEAM LEADER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, null);

        //WHEN
        boolean result = new RecruitmentProcessSpecification().isSatisfiedBy(recruitmentProcess);

        //THEN
        Assertions.assertTrue(result);
    }

    @Test
    void team_lead_fail() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12_000, "TEAM LEADER");
        Candidate candidate = new Candidate(50_000);
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, null);

        //WHEN
        boolean result = new RecruitmentProcessSpecification().isSatisfiedBy(recruitmentProcess);

        //THEN
        Assertions.assertFalse(result);
    }

    @Test
    void cleaner() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12_000, "CLEANER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, null, null);

        //WHEN
        boolean result = new RecruitmentProcessSpecification().isSatisfiedBy(recruitmentProcess);

        //THEN
        Assertions.assertTrue(result);
    }
}
