package com.klimek.ddd.patterns.specification;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class RecruitmentProcessTest {

    Candidate candidate = new Candidate(10000);
    SoftSkillsInterviewFeedback soft = new SoftSkillsInterviewFeedback(true);
    TechnicalInterviewFeedback technical = new TechnicalInterviewFeedback(true);
    TeamLeaderFeedback teamLeader = new TeamLeaderFeedback(true);

    @Test
    void recruitment_process_developer() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "DEVELOPER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, teamLeader);

        //WHEN
        boolean result = recruitmentProcess.isSuccess();

        //THEN
        Assertions.assertTrue(result);
    }


    @Test
    void recruitment_process_developer_fail() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "DEVELOPER");
        Candidate candidate = new Candidate(20000);
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, teamLeader);

        //WHEN
        boolean result = recruitmentProcess.isSuccess();

        //THEN
        Assertions.assertFalse(result);
    }

    @Test
    void recruitment_process_team_leader() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "TEAM LEADER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, technical, null);

        //WHEN
        boolean result = recruitmentProcess.isSuccess();

        //THEN
        Assertions.assertTrue(result);
    }

    @Test
    void recruitment_process_cleaner() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(12000, "CLEANER");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, soft, null, null);

        //WHEN
        boolean result = recruitmentProcess.isSuccess();

        //THEN
        Assertions.assertTrue(result);
    }

    @Test
    void recruitment_process_team_leader_cobol() {
        //GIVEN
        JobPosition jobPosition = new JobPosition(0, "COBOL");
        RecruitmentProcess recruitmentProcess = new RecruitmentProcess(candidate, jobPosition, null, technical, null);

        //WHEN
        boolean result = recruitmentProcess.isSuccess();

        //THEN
        Assertions.assertTrue(result);
    }
}
